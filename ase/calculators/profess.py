import numpy as np

from ase.calculators.calculator import Calculator
from ase.calculators.calculator import all_changes

class Profess(Calculator):
    """Profess calculator.
    """

    implemented_properties = ['energy', 'forces', 'stress']

    def __init__(self, profess_system):
        """Profess calculator implementation.
        """
        self.atoms = None  # copy of atoms object from last calculation
        self.results = {}  # calculated properties (energy, forces, ...)
        self.system = profess_system

    def calculate(self, atoms=None, properties=['energy'],
                  system_changes=all_changes):

        if atoms is not None:
            self.atoms = atoms.copy()

        # todo: do this more intelligently
        self.system.set_box(atoms.cell[:])
        self.system.move_ions(atoms.positions)
        self.system.minimize_energy()

        for p in properties:
            if p == 'energy':
                self.results['energy'] = self.system.energy()
            elif p == 'forces':
                # todo: remove the numpy conversion once it's obselete
                self.results['forces'] = np.array(self.system.forces())
            elif p == 'stress':
                # todo: remove the numpy conversion once it's obselete
                stress = np.array(self.system.stress())
                voight = np.empty(6)
                for i in range(3):
                    voight[i] = stress[i][i]
                voight[3] = stress[1,2]
                voight[4] = stress[0,2]
                voight[5] = stress[0,1]
                self.results['stress'] = voight
            else:
                print('can\'t calculate property:', p)
